#!/usr/bin/bash

# This code template is part of the arctools collection.
# 
# Copyright (C) 2021- CRTS
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

#!> INCLUDE delete.mod
#!> INCLUDE path-autoconfig.mod
#!> INCLUDE log.mod

#!> INCLUDE ../../../common.constants
#!> INCLUDE ../../../version

set_log_prefix "[$APP_NAME] "

declare TMP
TMP=$(mktemp -d)
if (( $? ));then
	error $E_FILE_CREATE "Failed to created temporary directory."
fi

TMPSRC="$TMP/src"
TMPTGT="$TMP/tgt"

replace_manifest_opt() {
	awk -v opt="$1" -v rep="$2" 'BEGIN {silent=0;} {
		if (silent) {
			if ($0 ~ "^\\[" ) {
				print rep
				print $0
				silent=0
			}
		} else {
			print
		}
		if ($1 == "["opt"]" )  {silent=1;}
	}' "$3" > "$3.awktmp"

	mv "$3.awktmp" "$3"
}

remove_manifest_logs() {
	sed -i "/^[[:blank:]]*\[$MANIFEST_LOGS\]/q" "$1"
}

test_targets() {
	local t
	local -i ret=0

	local -i len="${#tgt[@]}"
	local -i n
	for (( n = 0; n < len; n++ ));do
		del -rf "$TMPTGT"
		mkdir -p "$TMPTGT"

		t="${tgt[n]}"
		rsync --links --dirs "$t" "$TMPTGT" 2>/dev/null

		if [[ -n "$(find "$TMPTGT" -maxdepth 1 -mindepth 1 -print -quit)" ]];then
			warn $E_CUSTOM "Target already exists: $t"
			unset "tgt[n]"
			ret=1
		fi
	done

	return $ret
}

init() {
	rsync -r --include='/*/' --include='/.meta/*' --exclude='/.meta/*/*' --exclude='/*/*' "$src/" "$TMPSRC" 2>/dev/null

	local m

	if [[ -d "$TMPSRC/.meta" ]];then
		if [[ -n "$(find "$TMPSRC/.meta" -maxdepth 1 -type f -name 'manifest.*' -print -quit)" ]];then
			for m in "$TMPSRC"/.meta/manifest.*;do
				remove_manifest_logs "$m"
			done

			if ! test_targets;then
				if (( force ));then
					warn $E_CUSTOM "Some targets already exist."
				else
					error $E_CUSTOM "Some targets already exist."
				fi
			fi
		else
			error $E_FILE_MISSING "manifest" "Not an archive: $src"
		fi
	else
		error $E_FILE_MISSING ".meta" "Not an archive: $src"
	fi

	return 0
}

main() {
	local m

	local t
	for t in "${tgt[@]}";do
		local uuid=$(uuidgen)

		for m in "$TMPSRC"/.meta/manifest.*;do
			if [[ -z "$continuation" ]];then
				replace_manifest_opt "$MANIFEST_UUID" "$uuid" "$m"
			else
				replace_manifest_opt "$MANIFEST_CONTINUATION" "$continuation" "$m"
			fi
		done

		# upload modified archive to destination
		rsync -r "$TMPSRC/" "$t"
	done
}

declare continuation=
declare -i force=0

while getopts 'c:fv' o;do
	case $o in
	c)
		continuation="$OPTARG"
		;;
	f)
		force=1
		;;
	v) # print version and exit
		echo "$APP_NAME $VERSION"
		exit 0
		;;
	*)
		;;
	esac
done

shift $(( OPTIND - 1 ))

if (( $# < 1 ));then
	error $E_PARAM_MISSING "SOURCE"
fi

if (( $# < 2 ));then
	error $E_PARAM_MISSING "TARGET"
fi

declare -a tgt
declare -r src=$(norm_path "$1")

shift

while (( $# ));do
	tgt[${#tgt[@]}]=$(norm_path "$1")
	shift
done

init
if (( $? ));then
	error $E_INIT
fi

main

exit

