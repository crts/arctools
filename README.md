A collection of Bash scripts for personal use in order to create
incremental backups. The scripts use 'rsync' to create the backups.

There are three main scripts that are responsible
for the backups:

- arcinit (create and/or initialize an archive)
- arcdir (create incremental backups)
- snapdir (create incremental snapshots)

A few other scripts have been created which leverage the
funcionality of the main scripts.

These tools are published under the GPLv3 license. You should
have received a copy of the license.

