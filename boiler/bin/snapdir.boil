#!/usr/bin/bash

# This code template is part of the arctools collection.
# 
# Copyright (C) 2021- CRTS
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

#!> INCLUDE log.mod
#!> INCLUDE array.mod
#!> INCLUDE ../../common
#!> INCLUDE ../../version

declare -r bkp="$BACKUP.$SNAPDIR"
declare -r current="$CURRENT.$SNAPDIR"

declare -i FORCE=0
declare -i CHANGE_SNAPSHOT=0

declare linkdest=""

declare -i initstatus=0
declare -i updatetest=0

# Cleanup all temporary files and folders. Copy logs
# to final destination and release lock.
cleanup() {
	if (( ( ! updatetest) &&
		(initstatus == R_CHANGES || initstatus == R_UPDATES) ));then
		upload_logs
	fi

	remove_tmp
}

init() {
	common_init $SNAPDIR
	if (( $? ));then
		error $E_INIT_FAILED "Common initialization failed."
	fi

	if (( ! FORCE ));then
		check_update "${src[@]}" "$tgtdir" -- "${rsyncopts[@]}" "${flags[@]}" "--delete"
	else
		return $R_UPDATES
	fi
}

main() {
	local -a opts
	append_array2array opts rsyncopts
	append_array2array opts flags
	append_value2array opts "--delete"

	case $initstatus in
	$R_NO_CHANGES) # no updates, no changes
		log "No changes detected. No snapshot created."
		return $R_NO_CHANGES
		;;
	$R_CHANGES)
		# no special requirements
		log "Changes detected."
		if (( CHANGE_SNAPSHOT ));then
			new_snapshot
		fi
		;;
	$R_UPDATES)
		log "Updates detected."
		new_snapshot
		;;

	*)
		error $E_INIT_FAILED "Initialization failed."
		;;
	esac

	rsync "${opts[@]}" ${linkdest:+"$linkdest"} -i "${src[@]}" "$tgtdir" > "$logdir/sync.current" 

}

new_snapshot() {
	log "Create new snapshot."

	if [[ -n "$lastbkp" ]];then # not initial backup
		linkdest="--link-dest=../$lastbkp"
	fi

	local latestbkp="$bkp/$stamp"
	local newsnaptgt="$tgt/$latestbkp"

	rsync "${opts[@]}" ${linkdest:+"$linkdest"} -i "${src[@]}" "$newsnaptgt" > "$logdir/sync.snap" 

	linkdest="--link-dest=../$latestbkp"
}

while getopts "U:G:CFuvh" option;do
	case $option in
	U) # UUID of archive to use
		# TBD
		;;
	G) # archive group to use
		# TBD
		;;
	C)
		CHANGE_SNAPSHOT=1
		;;
	F)
		FORCE=1
		;;
	u)
		updatetest=1
		;;
	v) # print version and exit
		echo "$APP_NAME $VERSION"
		exit 0
		;;
	h) # print help and exit
		# TBD
		exit 0
		;;
	*)
		exit 1
		;;
	esac
done

shift $(( OPTIND - 1 ))

#!> INCLUDE ../../optshift

init
initstatus=$?
if (( updatetest ));then
	cleanup
	exit $initstatus
fi

main

cleanup

exit

