#!/usr/bin/bash

# This script is part of the arctools test collection.
# 
# Copyright (C) 2021- CRTS
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

resolve_path() {(
	unset CDPATH
	[[ -e "$1" ]] || return 1
	if [[ -d "$1" ]];then
		cd "$1" && pwd
	else
		local -r path=$(get_path "$1")
		local -r file=$(get_filename "$1")
		cd "$path" || return 1
		echo "$(pwd)/$file"
	fi
)}

is_explicit_path() {
	[[ ${1:0:1} == '/' || ${1:0:2} == './' || ${1:0:3} == '../' ]]
}

norm_path() {(
	shopt -s extglob

	if [[ -n "$1" ]];then
		local v="${1//+(\/)//}"

		if [[ "$v" == "/" ]];then
			echo "$v"
		else
			echo "${v%/}"
		fi
	fi
)}

get_path() {
	if [[ -n "$1" ]];then
		local w=$(norm_path "$1")
		local v="${w%/*}"

		if [[ "$v" == "$w" ]];then
			echo "."
		else
			if [[ -n "$v" ]];then
				echo "$v"
			else
				echo "/"
			fi
		fi
	fi
}

get_filename() {
	if [[ -n "$1" ]];then
		local v=$(norm_path "$1")
		v="${v##*/}"
		if [[ -z "$v" ]];then
			echo "/"
		else
			echo "$v"
		fi
	fi
}







declare -r E_PARAM_INVALID=10
declare -r E_OPTION_INVALID=11
declare -r E_OPTION_INCOMPATIBLE=12
declare -r E_PARAM_MISSING=15
declare -r E_FILE_MISSING=20
declare -r E_FILE_CORRUPT=22
declare -r E_FILES_NOT_EQUAL=25
declare -r E_FILE_CREATE=30
declare -r E_NO_DEVICE=31
declare -r E_EXECUTE_PERMISSION=40
declare -r E_INTEGER=50
declare -r E_DELETE_FORBIDDEN=60
declare -r E_INIT=70
declare -r E_CUSTOM=200
declare -r E_UNKNOWN=255


is_int() {
	[[ "$1" =~ ^[0-9]+$ ]]
}

is_true() {
	case "${1,,}" in
	y|yes|true|1)
		return 0
		;;
	*)
		return 1
		;;
	esac
}

is_false() {
	case "${1,,}" in
	n|no|false|0)
		return 0
		;;
	*)
		return 1
		;;
	esac
}


declare _log_prefix_="[$0] "

set_log_prefix() {
	_log_prefix_="$1"
}

log() {
	local -i _prefix_indent=0
	if [[ -n "$_log_prefix_" ]];then
		echo -n "$_log_prefix_" >&2
		_prefix_indent=${#_log_prefix_}
	fi

	printf "%s\n" "$1" >&2
	shift

	while (( $# > 0 ));do
		printf "%${_prefix_indent}s%s\n" '' "$1" >&2
		shift
	done
}

_err_msg() {
        local -r prefix="$1"
        local -r code=$2
        shift 2

        if ! is_int $code;then
                echo "Non-integer error code passed to error function." >&2
                return E_UNKNOWN
        fi

        local -a msg
        case $code in
        $E_OPTION_INVALID)
                msg[0]="$1"
                shift
                log "$prefix: Invalid option: ${msg[0]}" "$@"
                ;;
        $E_OPTION_INCOMPATIBLE)
                msg[0]="$1"
                msg[1]="$2"
                shift 2
                log "$prefix: Option '${msg[0]}' is incompatible with previous option: ${msg[1]}" "$@"
                ;;
        $E_PARAM_INVALID)
                msg[0]="$1"
                msg[1]="$2"
                shift 2
                log "$prefix: Option '${msg[0]}' has invalid parameter: ${msg[1]}" "$@"
                ;;
        $E_PARAM_MISSING)
                msg[0]="$1"
                shift
                log "$prefix: Mandatory parameter missing: ${msg[0]}" "$@"
                ;;
        $E_FILE_MISSING)
                msg[0]="$1"
                shift
                log "$prefix: File or folder not found: ${msg[0]}" "$@"
                ;;
        $E_FILE_CORRUPT)
                msg[0]="$1"
                shift
                log "$prefix: File corrupt: ${msg[0]}" "$@"
                ;;
        $E_FILES_NOT_EQUAL)
                msg[0]="$1"
                msg[1]="$2"
                shift 2
                log "$prefix: Files '${msg[0]}' and '${msg[1]}' are not equal" "$@"
                ;;
        $E_FILE_CREATE)
                msg[0]="$1"
                shift
                log "$prefix: File or folder creation failed: ${msg[0]}" "$@"
                ;;
        $E_NO_DEVICE)
                msg[0]="$1"
                shift
                log "$prefix: Device does not exist: ${msg[0]}" "$@"
                ;;
        $E_EXECUTE_PERMISSION)
                msg[0]="$1"
                shift
                log "$prefix: No execute permission: ${msg[0]}" "$@"
                ;;
        $E_DELETE_FORBIDDEN)
                msg[0]="$1"
                shift
                log "$prefix: Deletion forbidden: ${msg[0]}" "$@"
                ;;
        $E_INIT)
                log "$prefix: Initialization failed." "$@"
                ;;
        $E_INTEGER)
                msg[0]="$1"
                msg[1]="$2"
                shift 2
                log "$prefix: Cannot assign non-integer value '${msg[0]}' to integer-only variable: ${msg[1]}" "$@"
                ;;
        *)
                msg[0]="$1"
                shift
                log "$prefix: ${msg[0]}" "$@"
                ;;
        esac


        return $code
}

error() {
        local -r code=$1
        shift

        _err_msg "Error" $code "$@"

        exit $?
}

warn() {
        local -r code=$1
        shift

        _err_msg "Warning" $code "$@"

        return $?
}



declare -a _DELETION_PATHS_

add_del_paths() {
	local -i _last=${#_DELETION_PATHS_[@]}

	while (( $# ));do
		_DELETION_PATHS_[_last]=$(realpath "$1")
		(( _last++ )) || :
		shift
	done
}

set_del_paths() {
	_DELETION_PATHS_=()
	add_del_paths "$@"
}

reset_del_paths() {
	set_del_paths "/tmp"
}

reset_del_paths

del() {
	local _file

	local -a _option
	local -i _last=0
	while [[ "${1:0:1}" == "-" ]];do
		_option[_last]="$1"
		(( _last++ )) || :
		if [[ "$1" == "--" ]];then
			shift
			break
		fi
		shift
	done

	local _deldir
	local -a _list
	_last=0
	while (( $# ));do
		_file=$(realpath "$1")
		for _deldir in "${_DELETION_PATHS_[@]}";do
			if [[ "${_file#$_deldir}" != "$_file" ]];then
				_list[_last]="$_file"
				(( _last++ )) || :
				break
			fi
		done

		shift
	done

	if (( ${#_list[@]} ));then
		rm "${_option[@]}" "${_list[@]}"
	else
		return 1
	fi
}

delete() {
	local -r validator="$1"
	shift

	local option
	if [[ "$1" == "-r" ]];then
		option="-r"
		shift
	fi

	local file
	while (( $# ));do
		file=$(realpath "$1")
		if [[ "$file" != "${file/$validator/}" ]];then
			del $option "$file"
		else
			error "$E_DELETE_FORBIDDEN" "$file"
		fi

		shift
	done
}


shopt -s nullglob

declare -i EXITSTATUS=2

verdict() {
	case $EXITSTATUS in
	0)
		echo "${testname##*/}		PASS" | tee -a "$summary"
		;;
	1)
		echo "Result failed" >> "$logfile"
		echo "${testname##*/}		FAIL" | tee -a "$summary"
		;;
	*)
		echo "Erroneous result" >&2
		echo "${testname##*/}		ERROR" | tee -a "$summary"
		;;
	esac
}

trap 'verdict' EXIT




declare -r APP_PATH=$(resolve_path "$(get_path "$0")")
declare -r APP_NAME=$(get_filename "$0")
declare -r APP_INSTALL_PATH=$(resolve_path "$APP_PATH/..")

export_app_paths() {
	export APP_PATH
	export APP_NAME
	export APP_INSTALL_PATH
}

export_app_paths


declare ETC
declare LIB
declare SHARE
declare VAR

export_paths() {
	export ETC
	export LIB
	export SHARE
	export VAR
}

get_prefixed_path() {
	local target="$1"
	local indicator="$2"
	shift 2

	local -a prefix=( "$@" )
	local result

	local _d
	for _d in "${prefix[@]}";do
		if [[ -d "$_d/$target/$indicator" ]];then
			result="$_d/$target"
			break;
		fi
	done

	echo "$result"
}

set_paths() {
	local indicator="$1"

	shift
	local -a prefix=( "$@" )

	ETC=$(get_prefixed_path etc "$indicator" "${prefix[@]}")
	LIB=$(get_prefixed_path lib "$indicator" "${prefix[@]}")
	SHARE=$(get_prefixed_path share "$indicator" "${prefix[@]}")
	VAR=$(get_prefixed_path var "$indicator" "${prefix[@]}")
}

get_config() {
	local -r subdirname="$1"
	local -r conf="$2"

	if is_explicit_path "$conf";then
		echo "$conf"
		return
	fi

	local c
	for c in "$HOME/.config/$subdirname" "$HOME/.$subdirname/config" "$HOME/.$subdirname" "$ETC/$subdirname";do
		if [[ -e "$c/$conf" ]];then
			echo "$c/$conf"
			return 0
		fi
	done

	return 1
}

get_share() {
	local -r subdirname="$1"
	local -r share="$2"

	if is_explicit_path "$share";then
		echo "$share"
		return
	fi

	local s
	for s in "$HOME/.local/share/$subdirname" "$HOME/.$subdirname/share" "$HOME/.$subdirname" "$SHARE/$subdirname";do
		if [[ -e "$s/$share" ]];then
			echo "$s/$share"
			return 0
		fi
	done

	return 1
}

mk_user_confdir() {
	local -r confdir="$HOME/.config/$1"

	mkdir -p "$confdir"

	[[ -d "$confdir" ]]
}

mk_user_sharedir() {
	local -r sharedir="$HOME/.local/share/$1"

	mkdir -p "$sharedir"

	[[ -d "$sharedir" ]]
}

mk_user_dirs() {
	local -r subdirname="$1"
	local -i retval=0

	mk_user_confdir "$subdirname"
	retval=$?

	mk_user_sharedir "$subdirname"
	(( retval )) || retval=$?

	return $retval
}

mk_user_dirs_legacy() {
	local -r legacydir="$HOME/.$1/share"
	mkdir -p "$legacydir"

	[[ -d "$legacydir" ]]
}

set_paths "" "$APP_INSTALL_PATH" "/usr/local" "/usr" "/"
export_paths



create_folders() {
	local -r list="$1"
	local -r prefix=$(norm_path "$2")

	while read d;do
		mkdir -p "$prefix/$d"
	done < "$list"
}

create_files() {
	local -r list="$1"
	local -r prefix=$(norm_path "$2")

	local name
	local dir

	while read f;do
		name="$prefix/$f"
		dir=$(get_path "$name")
		mkdir -p "$d"
		touch "$name"
	done < "$list"
}

check_files_exist() {
	local -r prefix=$(norm_path "$2")

	local file

	while read f;do
		file="$prefix/$f"

		[[ -f "$file" ]] && echo "$file" || echo "$file" >&2
	done < "$1"
}

check_exist() {
	local -r prefix=$(norm_path "$2")

	local file

	while read f;do
		file="$prefix/$f"

		[[ -e "$file" ]] && echo "$file" || echo "$file" >&2
	done < "$1"
}

check_files_equal() {
	exec 3<"$1"
	exec 4<"$2"

	local -r prea=$(norm_path "$3")
	local -r preb=$(norm_path "$4")

	local fa
	local fb

	while read -u 3 a;do

		read -u 4 b

		fa="$prea/$a"
		fb="$preb/$b"

		if diff "$fa" "$fb">/dev/null;then
			echo "$fa|$fb"
		else
			echo "$fa|$fb" >&2
		fi

	done

	exec 3<&-
	exec 4<&-
}

check_modtime_equal() {
	exec 3<"$1"
	exec 4<"$2"

	local -r prea=$(norm_path "$3")
	local -r preb=$(norm_path "$4")

	local fa
	local fb

	while read -u 3 a;do

		read -u 4 b

		fa="$prea/$a"
		fb="$preb/$b"

		if [[ $(stat -c %Y "$fa") == $(stat -c %Y "$fb") ]];then
			echo "$fa|$fb"
		else
			echo "$fa|$fb" >&2
		fi

	done

	exec 3<&-
	exec 4<&-

}

dir_count_content() {(
	local folder="$1"

	shopt -s nullglob dotglob

	local content=( "$folder"/* )
	echo ${#content[@]}

)}

get_free_filename() {
	local name="$1"
	local -i max=${2:-100}
	local -i count=1

	while [[ -e "$name" ]];do
		name="$1.$count"
		(( count++ > max )) && return 1
	done

	echo "$name"

	return 0
}


declare -r APP_DIRNAME="arctools"

declare -r MANIFEST_VERSION="VERSION"
declare -r MANIFEST_GROUP="GROUP"
declare -r MANIFEST_UUID="UUID"
declare -r MANIFEST_CONTINUATION="CONTINUATION"
declare -r MANIFEST_STATUS="STATUS"
declare -r MANIFEST_ARCHIVE="ARCHIVE"
declare -r MANIFEST_EXCLUSIVE="EXCLUSIVE"
declare -r MANIFEST_FLAGS="FLAGS"
declare -r MANIFEST_WHITELIST="WHITELIST"
declare -r MANIFEST_BLACKLIST="BLACKLIST"
declare -r MANIFEST_DESCRIPTION="DESCRIPTION"
declare -r MANIFEST_LOGS="LOGS"

declare -r STATUS_ACTIVE="ACTIVE" # fully functional
declare -r STATUS_INACTIVE="INACTIVE" # reserved for future use
declare -r STATUS_SUSPENDED="SUSPENDED" # reading and writing prohibited
declare -r STATUS_READONLY="READONLY" # can only be used for restoration

declare -r R_NO_CHANGES=7
declare -r R_CHANGES=8
declare -r R_UPDATES=9

declare -r DEFAULT_FLAGS="-r -l -p -t -g -o -D"

declare -r RESERVED="RESERVED"

declare -r LOGS="logs"
declare -r MANIFEST_NAME="manifest"

declare -r SNAPDIR="snapdir"
declare -r ARCDIR="arcdir"

declare -r META=".meta"
declare -r BACKUP=".backup"
declare -r CURRENT="current"

declare -r logsuf="log"


if (( $(id -u) == 0 || $(id -g) == 0 ));then
	echo "Tests must not be run as root! Abort." >&2
	exit 1
fi

declare -r TESTUSERFILE="$APP_INSTALL_PATH/helper/testuser"
if [[ ! -f "$TESTUSERFILE" ]];then
	echo "No test user defined. Abort." >&2
	exit 1
fi

declare -r TESTUSER=$(head -n1 "$APP_INSTALL_PATH/helper/testuser")
if [[ "$USER" != "$TESTUSER" ]];then
	echo "Current user is not authorized to run tests. Abort." >&2
	exit 1
fi


declare -r test="$1"

if [[ ! -d "$test" ]];then
	echo "Error: No test directory specified." >&2
	exit 1
fi

declare REMOTE # address of remote machine
declare -a RMTTRANSFER # test folder to be transferred to remote machine
declare -r REMOTE_PREFIX="${RMTTGT:+$RMTTGT:}${DAEMON:+${RMTTGT:+:$DAEMON}}"

declare -r testname=$(basename $test)

WORKDIR="${WORKDIR:-$APP_PATH/results}"
declare -r logfile="$WORKDIR/$testname.log"
declare -r summary="$WORKDIR/summary.log"
declare -r base="$WORKDIR/$testname"

declare -r src="src"
declare -r tgt="tgt"
declare -r ref="ref"

declare -a SRC
declare TGT

. "$test/run"

set -a
if [[ -f "$APP_PATH/modify_window" ]];then
	. "$APP_PATH/modify_window"
elif [[ -f "$APP_PATH/../helper/modify_window" ]];then
	. "$APP_PATH/../helper/modify_window"
fi
set +a

MOD_WIN=${MOD_WIN:-0}

init_log() {
	echo "Description:" >> "$logfile"
	cat "$test/desc" >> "$logfile"

	echo "------------------------------------" >> "$logfile"
	echo "" >> "$logfile"

	echo "Initializing." >> "$logfile"
}

cleanup() {
	rm -rf "$base"
	rm -f "$logfile" # remove stale logfile
	sync

	if [[ -d "$base" ]];then
		error $E_CUSTOM "Failed to delete testarchive: $base"
	fi
}

folderstructure() {
	echo "structure: $1" >> "$logfile"
	echo "----------------------------------------------" >> "$logfile"
	if [[ -d "$1" ]];then
		"$APP_PATH/../helper/getinfo.sh" "$1" "$2" >> "$logfile"
	fi
}

getallfolderstructures() {
	sync
	echo "Displayed paths are relative to structure." >> "$logfile"
	echo "----------------------------------------------" >> "$logfile"
	echo "" >> "$logfile"
	for f in "${SRC[@]}" "$TGT" "$base/$ref";do
		folderstructure "$f" "$META"
		echo "----------------------------------------------" >> "$logfile"
	done
}

check_file_existance() {
	local -n reference="$1"
	local -r prefix=$(norm_path "$2")

	local i file
	for (( i=0; i<${#reference[@]}; i++ ));do
		file="$prefix/${reference[i]}"
		[[ -f "$file" ]] && echo "$file" || echo "$file" >&2
	done
}




fetch_remote() {
	if [[ -z "$REMOTE" ]];then
		return 0
	fi

	local rt
	for rt in "${RMTTRANSFER[@]}";do
		echo "Fetching remote folder: $REMOTE:$rt" >> "$logfile"
		rsync -aH "$REMOTE:${DAEMON:+:$DAEMON/}$rt/" "$rt" &>/dev/null
	done
}

delete_remote() {
	local rt
	for rt in "${RMTTRANSFER[@]}";do
		echo "Deleting remote test folder '$REMOTE:$rt'" >> "$logfile"
		ssh "$REMOTE" "rm -rf '$rt'"
	done
}

init_remote() {
	if [[ -n "$RMTTGT" && -n "$RMTSRC" ]];then
		echo "Must not define remote source and remote target" >> "$logfile"
		return 1
	fi

	if [[ -n "$RMTTGT" ]];then
		RMTTRANSFER[0]="$TGT"
		REMOTE="$RMTTGT"
	elif [[ -n "$RMTSRC" ]];then
		local s
		for s in "${SRC[@]}";do
			RMTTRANSFER[${#RMTTRANSFER[@]}]="$s"
		done
		REMOTE="$RMTSRC"
	fi

	if [[ -z "$REMOTE" ]];then
		return 0
	fi

	local rt
	for rt in "${RMTTRANSFER[@]}";do
		ssh "$REMOTE" "mkdir -p '$rt'"
	done

	delete_remote

	for rt in "${RMTTRANSFER[@]}";do
		echo "Copying local folder '$rt' to remote destination: $REMOTE:$rt" >> "$logfile"
		rsync -aH "$rt/" "$REMOTE:${DAEMON:+:$DAEMON/}$rt" &>/dev/null
		(( $? )) && return 1

		rm -rf "$rt"
	done
}


checkresult() {
	fetch_remote
	if (( $? ));then
		echo "Failed to copy result directory from remote host" >> "$logfile"
		return 20
	fi

	local -r reffiles=$(find "$base/$ref" -type d -name "$META" -prune -o -type f -printf %P'\n')
	echo "checking if all reference files are in '$base'" >> "$logfile"
	exist=$(check_files_exist <(echo "$reffiles") "$base" 2>&1 >/dev/null)

	[[ -n "$exist" ]] && ret=1

	local reffile
	local tgtfile
	while read f;do
		reffile="$base/$ref/$f"
		tgtfile="$base/$f"

		if ! diff "$reffile" "$tgtfile";then
			echo "$f: Contents do not match." >> "$logfile"
			ret=1
		fi
	done <<< "$reffiles"

	if (( ret ));then
		echo "Wrong results." >> "$logfile"
	else
		echo "Results OK." >> "$logfile"
	fi

	return $ret
}


init() {
	cleanup

	mkdir -p "$base"

	init_log

	local dst
	for t in "$test"/template.*;do
		dst="$base/${t##*.}"
		mkdir -p "$dst"

		"$APP_PATH/../helper/mktest" -d REMOTE_PREFIX="$REMOTE_PREFIX" "$t" "$dst" ""
	done

	sync

	TGT=$(realpath "$base/$tgt"*)
	OUT=$(realpath "$base")

	echo "Initial folder structures:" >> "$logfile"
	echo "---------------------------" >> "$logfile"
	getallfolderstructures

	init_remote
	if (( $? ));then
		echo "Failed to copy test directory to remote host" >> "$logfile"
		return 1
	fi

	arcinit

	echo "Initialization complete" >> "$logfile"
}

runtest() {
	init

	if (( $? ));then
		echo "Error during initialization!" >&2
		exit 1
	fi

	echo >> "$logfile"
	echo "running $prog" >> "$logfile"

	run "${REMOTE_PREFIX}$TGT" && :


		echo >> "$logfile"

		echo "final folder structures:" >> "$logfile"
		echo "---------------------------" >> "$logfile"
		getallfolderstructures

		echo "checking results" >> "$logfile"

		checkresult && :
		local res=$?
		if (( res ));then
			if (( res != 20 ));then
				EXITSTATUS=1
			fi
		else
			EXITSTATUS=0
		fi

}

runtest

exit

