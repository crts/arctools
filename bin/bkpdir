#!/usr/bin/bash

# Bkpdir is part of the arctools extra collection.
# 
# Copyright (C) 2021- CRTS
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

resolve_path() {(
	unset CDPATH
	[[ -e "$1" ]] || return 1
	if [[ -d "$1" ]];then
		cd "$1" && pwd
	else
		local -r path=$(get_path "$1")
		local -r file=$(get_filename "$1")
		cd "$path" || return 1
		echo "$(pwd)/$file"
	fi
)}

is_explicit_path() {
	[[ ${1:0:1} == '/' || ${1:0:2} == './' || ${1:0:3} == '../' ]]
}

norm_path() {(
	shopt -s extglob

	if [[ -n "$1" ]];then
		local v="${1//+(\/)//}"

		if [[ "$v" == "/" ]];then
			echo "$v"
		else
			echo "${v%/}"
		fi
	fi
)}

get_path() {
	if [[ -n "$1" ]];then
		local w=$(norm_path "$1")
		local v="${w%/*}"

		if [[ "$v" == "$w" ]];then
			echo "."
		else
			if [[ -n "$v" ]];then
				echo "$v"
			else
				echo "/"
			fi
		fi
	fi
}

get_filename() {
	if [[ -n "$1" ]];then
		local v=$(norm_path "$1")
		v="${v##*/}"
		if [[ -z "$v" ]];then
			echo "/"
		else
			echo "$v"
		fi
	fi
}





declare -r E_PARAM_INVALID=10
declare -r E_OPTION_INVALID=11
declare -r E_OPTION_INCOMPATIBLE=12
declare -r E_PARAM_MISSING=15
declare -r E_FILE_MISSING=20
declare -r E_FILE_CORRUPT=22
declare -r E_FILES_NOT_EQUAL=25
declare -r E_FILE_CREATE=30
declare -r E_NO_DEVICE=31
declare -r E_EXECUTE_PERMISSION=40
declare -r E_INTEGER=50
declare -r E_DELETE_FORBIDDEN=60
declare -r E_INIT=70
declare -r E_CUSTOM=200
declare -r E_UNKNOWN=255


is_int() {
	[[ "$1" =~ ^[0-9]+$ ]]
}

is_true() {
	case "${1,,}" in
	y|yes|true|1)
		return 0
		;;
	*)
		return 1
		;;
	esac
}

is_false() {
	case "${1,,}" in
	n|no|false|0)
		return 0
		;;
	*)
		return 1
		;;
	esac
}


declare _log_prefix_="[$0] "

set_log_prefix() {
	_log_prefix_="$1"
}

log() {
	local -i _prefix_indent=0
	if [[ -n "$_log_prefix_" ]];then
		echo -n "$_log_prefix_" >&2
		_prefix_indent=${#_log_prefix_}
	fi

	printf "%s\n" "$1" >&2
	shift

	while (( $# > 0 ));do
		printf "%${_prefix_indent}s%s\n" '' "$1" >&2
		shift
	done
}

_err_msg() {
        local -r prefix="$1"
        local -r code=$2
        shift 2

        if ! is_int $code;then
                echo "Non-integer error code passed to error function." >&2
                return E_UNKNOWN
        fi

        local -a msg
        case $code in
        $E_OPTION_INVALID)
                msg[0]="$1"
                shift
                log "$prefix: Invalid option: ${msg[0]}" "$@"
                ;;
        $E_OPTION_INCOMPATIBLE)
                msg[0]="$1"
                msg[1]="$2"
                shift 2
                log "$prefix: Option '${msg[0]}' is incompatible with previous option: ${msg[1]}" "$@"
                ;;
        $E_PARAM_INVALID)
                msg[0]="$1"
                msg[1]="$2"
                shift 2
                log "$prefix: Option '${msg[0]}' has invalid parameter: ${msg[1]}" "$@"
                ;;
        $E_PARAM_MISSING)
                msg[0]="$1"
                shift
                log "$prefix: Mandatory parameter missing: ${msg[0]}" "$@"
                ;;
        $E_FILE_MISSING)
                msg[0]="$1"
                shift
                log "$prefix: File or folder not found: ${msg[0]}" "$@"
                ;;
        $E_FILE_CORRUPT)
                msg[0]="$1"
                shift
                log "$prefix: File corrupt: ${msg[0]}" "$@"
                ;;
        $E_FILES_NOT_EQUAL)
                msg[0]="$1"
                msg[1]="$2"
                shift 2
                log "$prefix: Files '${msg[0]}' and '${msg[1]}' are not equal" "$@"
                ;;
        $E_FILE_CREATE)
                msg[0]="$1"
                shift
                log "$prefix: File or folder creation failed: ${msg[0]}" "$@"
                ;;
        $E_NO_DEVICE)
                msg[0]="$1"
                shift
                log "$prefix: Device does not exist: ${msg[0]}" "$@"
                ;;
        $E_EXECUTE_PERMISSION)
                msg[0]="$1"
                shift
                log "$prefix: No execute permission: ${msg[0]}" "$@"
                ;;
        $E_DELETE_FORBIDDEN)
                msg[0]="$1"
                shift
                log "$prefix: Deletion forbidden: ${msg[0]}" "$@"
                ;;
        $E_INIT)
                log "$prefix: Initialization failed." "$@"
                ;;
        $E_INTEGER)
                msg[0]="$1"
                msg[1]="$2"
                shift 2
                log "$prefix: Cannot assign non-integer value '${msg[0]}' to integer-only variable: ${msg[1]}" "$@"
                ;;
        *)
                msg[0]="$1"
                shift
                log "$prefix: ${msg[0]}" "$@"
                ;;
        esac


        return $code
}

error() {
        local -r code=$1
        shift

        _err_msg "Error" $code "$@"

        exit $?
}

warn() {
        local -r code=$1
        shift

        _err_msg "Warning" $code "$@"

        return $?
}





declare -r APP_PATH=$(resolve_path "$(get_path "$0")")
declare -r APP_NAME=$(get_filename "$0")
declare -r APP_INSTALL_PATH=$(resolve_path "$APP_PATH/..")

export_app_paths() {
	export APP_PATH
	export APP_NAME
	export APP_INSTALL_PATH
}

export_app_paths


declare ETC
declare LIB
declare SHARE
declare VAR

export_paths() {
	export ETC
	export LIB
	export SHARE
	export VAR
}

get_prefixed_path() {
	local target="$1"
	local indicator="$2"
	shift 2

	local -a prefix=( "$@" )
	local result

	local _d
	for _d in "${prefix[@]}";do
		if [[ -d "$_d/$target/$indicator" ]];then
			result="$_d/$target"
			break;
		fi
	done

	echo "$result"
}

set_paths() {
	local indicator="$1"

	shift
	local -a prefix=( "$@" )

	ETC=$(get_prefixed_path etc "$indicator" "${prefix[@]}")
	LIB=$(get_prefixed_path lib "$indicator" "${prefix[@]}")
	SHARE=$(get_prefixed_path share "$indicator" "${prefix[@]}")
	VAR=$(get_prefixed_path var "$indicator" "${prefix[@]}")
}

get_config() {
	local -r subdirname="$1"
	local -r conf="$2"

	if is_explicit_path "$conf";then
		echo "$conf"
		return
	fi

	local c
	for c in "$HOME/.config/$subdirname" "$HOME/.$subdirname/config" "$HOME/.$subdirname" "$ETC/$subdirname";do
		if [[ -e "$c/$conf" ]];then
			echo "$c/$conf"
			return 0
		fi
	done

	return 1
}

get_share() {
	local -r subdirname="$1"
	local -r share="$2"

	if is_explicit_path "$share";then
		echo "$share"
		return
	fi

	local s
	for s in "$HOME/.local/share/$subdirname" "$HOME/.$subdirname/share" "$HOME/.$subdirname" "$SHARE/$subdirname";do
		if [[ -e "$s/$share" ]];then
			echo "$s/$share"
			return 0
		fi
	done

	return 1
}

mk_user_confdir() {
	local -r confdir="$HOME/.config/$1"

	mkdir -p "$confdir"

	[[ -d "$confdir" ]]
}

mk_user_sharedir() {
	local -r sharedir="$HOME/.local/share/$1"

	mkdir -p "$sharedir"

	[[ -d "$sharedir" ]]
}

mk_user_dirs() {
	local -r subdirname="$1"
	local -i retval=0

	mk_user_confdir "$subdirname"
	retval=$?

	mk_user_sharedir "$subdirname"
	(( retval )) || retval=$?

	return $retval
}

mk_user_dirs_legacy() {
	local -r legacydir="$HOME/.$1/share"
	mkdir -p "$legacydir"

	[[ -d "$legacydir" ]]
}

set_paths "" "$APP_INSTALL_PATH" "/usr/local" "/usr" "/"
export_paths

declare -r APP_DIRNAME="arctools"

declare -r MANIFEST_VERSION="VERSION"
declare -r MANIFEST_GROUP="GROUP"
declare -r MANIFEST_UUID="UUID"
declare -r MANIFEST_CONTINUATION="CONTINUATION"
declare -r MANIFEST_STATUS="STATUS"
declare -r MANIFEST_ARCHIVE="ARCHIVE"
declare -r MANIFEST_EXCLUSIVE="EXCLUSIVE"
declare -r MANIFEST_FLAGS="FLAGS"
declare -r MANIFEST_WHITELIST="WHITELIST"
declare -r MANIFEST_BLACKLIST="BLACKLIST"
declare -r MANIFEST_DESCRIPTION="DESCRIPTION"
declare -r MANIFEST_LOGS="LOGS"

declare -r STATUS_ACTIVE="ACTIVE" # fully functional
declare -r STATUS_INACTIVE="INACTIVE" # reserved for future use
declare -r STATUS_SUSPENDED="SUSPENDED" # reading and writing prohibited
declare -r STATUS_READONLY="READONLY" # can only be used for restoration

declare -r R_NO_CHANGES=7
declare -r R_CHANGES=8
declare -r R_UPDATES=9

declare -r DEFAULT_FLAGS="-r -l -p -t -g -o -D"

declare -r RESERVED="RESERVED"

declare -r LOGS="logs"
declare -r MANIFEST_NAME="manifest"

declare -r SNAPDIR="snapdir"
declare -r ARCDIR="arcdir"

declare -r META=".meta"
declare -r BACKUP=".backup"
declare -r CURRENT="current"

declare -r logsuf="log"

declare -r VERSION=1.0

declare -a OPTIONS

main() {
	local -i ret
	"$APP_PATH"/$ARCDIR "${OPTIONS[@]}" "${src[@]}" "$tgt" -- "${rsyncopts[@]}"

	ret=$?

	if (( ret ));then
		if (( ret == R_NO_CHANGES ));then
			log "No changes. Skipping snapshot."
			return $R_NO_CHANGES
		fi

		warn $E_CUSTOM "Error while archiving. Will try to create snapshot anyway."
	fi

	"$APP_PATH"/$SNAPDIR -F "${OPTIONS[@]}" "${src[@]}" "$tgt" -- "${rsyncopts[@]}" --link-dest="../../$CURRENT.$ARCDIR"
}

while getopts "U:G:vh" o;do
	case $o in
	U) # UUID of archive to use
		OPTIONS[${#OPTIONS[@]}]="-U"
		OPTIONS[${#OPTIONS[@]}]="$OPTARG"
		;;
	G) # archive group to use
		OPTIONS[${#OPTIONS[@]}]="-G"
		OPTIONS[${#OPTIONS[@]}]="$OPTARG"
		;;
	v) # print version and exit
		echo "$APP_NAME $VERSION"
		exit 0
		;;
	h) # print help and exit
		echo "bkpdir [-U UUID] [-G GROUP] [-v] [-h] SOURCE ... TARGET" >&2
		exit 0
		;;
	*)
		echo "Unknown option: $o" >&2
		exit 1
		;;
	esac
done

shift $(( OPTIND - 1 ))

shopt -s extglob

declare -a src
declare tgt

if (( $# < 1 ));then
	error $E_PARAM_MISSING "SOURCE"
elif (( $# < 2 ));then
	error $E_PARAM_MISSING "TARGET"
fi

while (( $# > 1 ));do
	if [[ "$2" == "--" ]];then
		break
	fi

	src[${#src[@]}]=$(norm_path "$1")
	shift
done

tgt=$(norm_path "$1")
shift

if [[ "$1" == "--" ]];then
	shift
fi

declare -a rsyncopts=( "$@" )


main

