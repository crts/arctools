#!/usr/bin/bash

# Gitsnapinit is part of the arctools extra collection.
# 
# Copyright (C) 2021- CRTS
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

resolve_path() {(
	unset CDPATH
	[[ -e "$1" ]] || return 1
	if [[ -d "$1" ]];then
		cd "$1" && pwd
	else
		local -r path=$(get_path "$1")
		local -r file=$(get_filename "$1")
		cd "$path" || return 1
		echo "$(pwd)/$file"
	fi
)}

is_explicit_path() {
	[[ ${1:0:1} == '/' || ${1:0:2} == './' || ${1:0:3} == '../' ]]
}

norm_path() {(
	shopt -s extglob

	if [[ -n "$1" ]];then
		local v="${1//+(\/)//}"

		if [[ "$v" == "/" ]];then
			echo "$v"
		else
			echo "${v%/}"
		fi
	fi
)}

get_path() {
	if [[ -n "$1" ]];then
		local w=$(norm_path "$1")
		local v="${w%/*}"

		if [[ "$v" == "$w" ]];then
			echo "."
		else
			if [[ -n "$v" ]];then
				echo "$v"
			else
				echo "/"
			fi
		fi
	fi
}

get_filename() {
	if [[ -n "$1" ]];then
		local v=$(norm_path "$1")
		v="${v##*/}"
		if [[ -z "$v" ]];then
			echo "/"
		else
			echo "$v"
		fi
	fi
}







declare -r APP_PATH=$(resolve_path "$(get_path "$0")")
declare -r APP_NAME=$(get_filename "$0")
declare -r APP_INSTALL_PATH=$(resolve_path "$APP_PATH/..")

export_app_paths() {
	export APP_PATH
	export APP_NAME
	export APP_INSTALL_PATH
}

export_app_paths


declare ETC
declare LIB
declare SHARE
declare VAR

export_paths() {
	export ETC
	export LIB
	export SHARE
	export VAR
}

get_prefixed_path() {
	local target="$1"
	local indicator="$2"
	shift 2

	local -a prefix=( "$@" )
	local result

	local _d
	for _d in "${prefix[@]}";do
		if [[ -d "$_d/$target/$indicator" ]];then
			result="$_d/$target"
			break;
		fi
	done

	echo "$result"
}

set_paths() {
	local indicator="$1"

	shift
	local -a prefix=( "$@" )

	ETC=$(get_prefixed_path etc "$indicator" "${prefix[@]}")
	LIB=$(get_prefixed_path lib "$indicator" "${prefix[@]}")
	SHARE=$(get_prefixed_path share "$indicator" "${prefix[@]}")
	VAR=$(get_prefixed_path var "$indicator" "${prefix[@]}")
}

get_config() {
	local -r subdirname="$1"
	local -r conf="$2"

	if is_explicit_path "$conf";then
		echo "$conf"
		return
	fi

	local c
	for c in "$HOME/.config/$subdirname" "$HOME/.$subdirname/config" "$HOME/.$subdirname" "$ETC/$subdirname";do
		if [[ -e "$c/$conf" ]];then
			echo "$c/$conf"
			return 0
		fi
	done

	return 1
}

get_share() {
	local -r subdirname="$1"
	local -r share="$2"

	if is_explicit_path "$share";then
		echo "$share"
		return
	fi

	local s
	for s in "$HOME/.local/share/$subdirname" "$HOME/.$subdirname/share" "$HOME/.$subdirname" "$SHARE/$subdirname";do
		if [[ -e "$s/$share" ]];then
			echo "$s/$share"
			return 0
		fi
	done

	return 1
}

mk_user_confdir() {
	local -r confdir="$HOME/.config/$1"

	mkdir -p "$confdir"

	[[ -d "$confdir" ]]
}

mk_user_sharedir() {
	local -r sharedir="$HOME/.local/share/$1"

	mkdir -p "$sharedir"

	[[ -d "$sharedir" ]]
}

mk_user_dirs() {
	local -r subdirname="$1"
	local -i retval=0

	mk_user_confdir "$subdirname"
	retval=$?

	mk_user_sharedir "$subdirname"
	(( retval )) || retval=$?

	return $retval
}

mk_user_dirs_legacy() {
	local -r legacydir="$HOME/.$1/share"
	mkdir -p "$legacydir"

	[[ -d "$legacydir" ]]
}

set_paths "" "$APP_INSTALL_PATH" "/usr/local" "/usr" "/"
export_paths


declare -r APP_DIRNAME="arctools"

declare -r MANIFEST_VERSION="VERSION"
declare -r MANIFEST_GROUP="GROUP"
declare -r MANIFEST_UUID="UUID"
declare -r MANIFEST_CONTINUATION="CONTINUATION"
declare -r MANIFEST_STATUS="STATUS"
declare -r MANIFEST_ARCHIVE="ARCHIVE"
declare -r MANIFEST_EXCLUSIVE="EXCLUSIVE"
declare -r MANIFEST_FLAGS="FLAGS"
declare -r MANIFEST_WHITELIST="WHITELIST"
declare -r MANIFEST_BLACKLIST="BLACKLIST"
declare -r MANIFEST_DESCRIPTION="DESCRIPTION"
declare -r MANIFEST_LOGS="LOGS"

declare -r STATUS_ACTIVE="ACTIVE" # fully functional
declare -r STATUS_INACTIVE="INACTIVE" # reserved for future use
declare -r STATUS_SUSPENDED="SUSPENDED" # reading and writing prohibited
declare -r STATUS_READONLY="READONLY" # can only be used for restoration

declare -r R_NO_CHANGES=7
declare -r R_CHANGES=8
declare -r R_UPDATES=9

declare -r DEFAULT_FLAGS="-r -l -p -t -g -o -D"

declare -r RESERVED="RESERVED"

declare -r LOGS="logs"
declare -r MANIFEST_NAME="manifest"

declare -r SNAPDIR="snapdir"
declare -r ARCDIR="arcdir"

declare -r META=".meta"
declare -r BACKUP=".backup"
declare -r CURRENT="current"

declare -r logsuf="log"

declare -r VERSION=1.0

declare -r ARCINIT="$APP_PATH/arcinit"

if [[ ! -x "$ARCINIT" ]];then
	echo "Archive initialization program '$ARCINIT' not executable." >&2
	exit 1
fi

declare -a options
declare -i index=0
while (( $# > 1 ));do
	options[index]="$1"
	(( index++ )) || :
	shift
done

declare uuid
uuid=$(uuidgen)

declare -r tgt="$1"

"$ARCINIT" -U "$uuid" "${options[@]}" -a $SNAPDIR -e0 "$tgt"
if (( $? ));then
	echo "Failed during '$SNAPDIR' initialization." >&2
	exit 2
fi

"$ARCINIT" -U "$uuid" "${options[@]}" -F -a gitsnap "$tgt"
if (( $? ));then
	echo "Failed during 'gitsnap' initialization." >&2
	exit 2
fi

