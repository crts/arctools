## [1.0] 2021-03-04
### Added
- Started this CHANGELOG

### Removed
- Butterfingers is no longer part of arctools

### Fixed
- Fixed handling of special characters for arcscheme and its helper script
  'hook_available_default'
- Arcsnap does not skip top level symlinks any more
- Version information for all scripts

### Changed
- Removed superfluous comments and messages
- Licence notice in scripts
- Adjusted behaviour of snapdir to preserve empty directories in backups


